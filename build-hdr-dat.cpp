#include "WinInclude.h"
#include <string>
#include <vector>
#include <map>
#include <filesystem>
#include <fstream>
#include "commandline.h"
#include "converter.h"
#include <format>

using namespace std;
using namespace std::filesystem;

void ErrorMessage(wstring const &msg) {
    MessageBoxW(NULL, msg.c_str(), L"Error", MB_ICONERROR);
}

path TempPath() {
    static path tempPath = temp_directory_path() / "build_hdr_dat";
    return tempPath;
}

void CreateTempDirectory() {
    error_code ec;
    remove_all(TempPath(), ec);
    create_directories(TempPath(), ec);
}

void DeleteTempDirectory() {
    error_code ec;
    remove_all(TempPath(), ec);
}

std::filesystem::path GetExePath() {
    static constexpr auto INITIAL_BUFFER_SIZE = MAX_PATH;
    static constexpr auto MAX_ITERATIONS = 7;
    std::wstring ret;
    auto bufferSize = INITIAL_BUFFER_SIZE;
    for (size_t iterations = 0; iterations < MAX_ITERATIONS; ++iterations) {
        ret.resize(bufferSize);
        auto charsReturned = GetModuleFileNameW(NULL, &ret[0], bufferSize);
        if (charsReturned < ret.length()) {
            ret.resize(charsReturned);
            return ret;
        }
        else
            bufferSize *= 2;
    }
    return std::filesystem::path();
}

std::filesystem::path GetExeDir() {
    auto exePath = GetExePath();
    if (exePath.has_parent_path())
        return exePath.parent_path();
    return std::filesystem::path();
}

void Trim(wstring &str) {
    size_t start = str.find_first_not_of(L" \t\r\n");
    if (start != wstring::npos)
        str = str.substr(start);
    size_t end = str.find_last_not_of(L" \t\r\n");
    if (end != wstring::npos)
        str = str.substr(0, end + 1);
}

vector<wstring> Split(wstring const &line, wchar_t delim, bool trim, bool skipEmpty, bool quotesHavePriority) {
    vector<wstring> result;
    wstring currStr;
    auto AddStr = [&, trim, skipEmpty]() {
        if (trim)
            Trim(currStr);
        if (!skipEmpty || !currStr.empty())
            result.push_back(currStr);
        currStr.clear();
    };
    bool inQuotes = false;
    for (size_t i = 0; i < line.length(); i++) {
        auto c = line[i];
        if (c == L'\r' || (delim != L'\n' && c == L'\n'))
            break;
        if (!inQuotes) {
            if (quotesHavePriority && c == L'"')
                inQuotes = true;
            else if (c == delim)
                AddStr();
            else
                currStr += c;
        }
        else {
            if (c == '"')
                inQuotes = false;
            else
                currStr += c;
        }
    }
    AddStr();
    return result;
}

unsigned int GetNumBytesToAlign(unsigned int offset, unsigned int alignment) {
    unsigned int m = offset % alignment;
    return (m > 0) ? (alignment - m) : 0;
}

unsigned int GetAligned(unsigned int offset, unsigned int alignment) {
    return offset + GetNumBytesToAlign(offset, alignment);
}

struct Sample {
    path source;
    vector<unsigned char> parameters;
    unsigned short offset = 0;
};

struct Bank {
    unsigned short type = 0;
    int subID = -1;
    unsigned char cycleLength = 0;
    unsigned char sampleRepeat = 0;
    unsigned int numSubBanks = 0;
    vector<Sample> samples;
    unsigned char blockSize = 0;
    unsigned short bankBlocks = 0;
    path filePath;
};

template<typename T> void SwapEndian(T &value) {}

void SwapEndian(unsigned int &value) {
    value = _byteswap_ulong(value);
}

void SwapEndian(unsigned short &value) {
    value = _byteswap_ushort(value);
}

void SwapEndian(int &value) {
    value = _byteswap_ulong(value);
}

void SwapEndian(short &value) {
    value = _byteswap_ushort(value);
}

class Writer {
    FILE *mpFile = nullptr;
    bool mBigEndian = false;
    unsigned int mStartPosition = 0;
public:
    Writer(FILE *file, bool bigEndian) {
        mpFile = file;
        mBigEndian = bigEndian;
        mStartPosition = ftell(mpFile);
    }

    void Write(unsigned char const *data, unsigned int size) {
        fwrite(data, 1, size, mpFile);
    }

    void Write(char const *data, unsigned int size) {
        fwrite(data, 1, size, mpFile);
    }

    void Write(unsigned char value, unsigned int count) {
        for (unsigned int i = 0; i < count; i++)
            Write(value);
    }

    unsigned int GetBytesWritten() {
        return ftell(mpFile) - mStartPosition;
    }

    template<typename T>
    void Write(T value) {
        if (mBigEndian)
            SwapEndian(value);
        fwrite((void *)&value, sizeof(T), 1, mpFile);
    }

    template<typename T>
    void WriteBigEndian(T value) {
        SwapEndian(value);
        fwrite((void *)&value, sizeof(T), 1, mpFile);
    }

    void Align(unsigned int alignment, unsigned char value = 0) {
        Write(value, GetNumBytesToAlign(ftell(mpFile), alignment));
    }
};

class Reader {
    FILE *mpFile = nullptr;
    bool mBigEndian = false;
public:
    Reader(FILE *file, bool bigEndian) {
        mpFile = file;
        mBigEndian = bigEndian;
    }

    void Read(unsigned char *data, unsigned int size) {
        fread(data, 1, size, mpFile);
    }

    void Read(char *data, unsigned int size) {
        fwrite(data, 1, size, mpFile);
    }

    void Skip(unsigned int count) {
        if (count > 0)
            fseek(mpFile, count, SEEK_CUR);
    }

    template<typename T>
    void Read(T &value) {
        fread((void *)&value, sizeof(T), 1, mpFile);
        if (mBigEndian)
            SwapEndian(value);
    }

    template<typename T>
    T Read() {
        T value {};
        fread((void *)&value, sizeof(T), 1, mpFile);
        if (mBigEndian)
            SwapEndian(value);
        return value;
    }

    template<typename T>
    void ReadBigEndian(T &value) {
        fwrite((void *)&value, sizeof(T), 1, mpFile);
        SwapEndian(value);
    }

    template<typename T>
    T ReadBigEndian() {
        T value {};
        fwrite((void *)&value, sizeof(T), 1, mpFile);
        SwapEndian(value);
        return value;
    }

    void Align(unsigned int alignment) {
        Skip(GetNumBytesToAlign(ftell(mpFile), alignment));
    }
};

void WriteBankHeader(Bank &bank, unsigned int version, FILE *f, bool bigEndian, bool datFile) {
    Writer w(f, bigEndian);
    if (datFile)
        w.Write("HrIn", 4);
    w.Write(bank.type);
    if (version == 1)
        w.Write((unsigned short)bank.subID);
    unsigned char numParameters = 0;
    for (auto const &s : bank.samples) {
        if (s.parameters.size() > numParameters)
            numParameters = s.parameters.size();
    }
    vector<unsigned int> parameterMask(numParameters, 0);
    for (auto const &s : bank.samples) {
        for (unsigned int p = 0; p < numParameters; p++) {
            unsigned char parameterValue = (p < s.parameters.size()) ? s.parameters[p] : 0;
            parameterMask[p] |= (1 << parameterValue);
        }
    }
    bool hasCycleBits = bank.cycleLength > 0;
    w.Write<unsigned char>((numParameters & 0x7F) | (hasCycleBits << 7));
    unsigned char numSamples = bank.samples.size();
    w.Write(numSamples);
    if (version == 2)
        w.Write(bank.subID);
    w.Write(bank.sampleRepeat);
    w.Write(bank.blockSize);
    w.Write(bank.bankBlocks);
    if (version == 1)
        w.Write((unsigned short)bank.numSubBanks);
    else
        w.Write(bank.numSubBanks);
    for (auto const &s : bank.samples) {
        w.WriteBigEndian(s.offset);
        for (unsigned int p = 0; p < numParameters; p++) {
            unsigned char parameterValue = (p < s.parameters.size()) ? s.parameters[p] : 0;
            w.Write(parameterValue);
        }
    }
    w.Align(4, 0x70);
    for (auto const &p : parameterMask)
        w.Write(p);
    if (hasCycleBits) {
        w.Write(bank.cycleLength);
        w.Write((unsigned char)0, (numSamples + 7) / 8);
    }
    if (bank.sampleRepeat > 0) {
        w.Write<unsigned char>(0);
        w.Write(0xFF, bank.sampleRepeat);
    }
    w.Align(4, 0x70);
    if (datFile) {
        w.Write("HrSz", 4);
        w.Write(w.GetBytesWritten() - 8);
    }
};

void ReadBankHeader(Bank &bank, unsigned int version, FILE *f, bool bigEndian) {
    Reader r(f, bigEndian);
    r.Read(bank.type);
    if (version == 1) {
        unsigned short sSubID = r.Read<unsigned short>();
        if (sSubID == (unsigned short)-1)
            bank.subID = -1;
        else
            bank.subID = sSubID;
    }
    unsigned char parmFlags = r.Read<unsigned char>();
    unsigned int numParameters = parmFlags & 0x7F;
    bool hasCycleBits = (parmFlags >> 7) & 1;
    unsigned char numSamples = 0;
    r.Read(numSamples);
    bank.samples.resize(numSamples);
    if (version == 2)
        r.Read(bank.subID);
    r.Read(bank.sampleRepeat);
    r.Read(bank.blockSize);
    r.Read(bank.bankBlocks);
    if (version == 1)
        bank.numSubBanks = r.Read<unsigned short>();
    else {
        bank.numSubBanks = r.Read<unsigned int>();
    }
    for (auto &s : bank.samples) {
        r.ReadBigEndian(s.offset);
        if (numParameters > 0) {
            s.parameters.resize(numParameters);
            r.Read(s.parameters.data(), numParameters);
        }
    }
    r.Align(4);
    r.Skip(numParameters * 4);
    if (hasCycleBits)
        r.Read(bank.cycleLength);
};

void WriteBankToXml(Bank const &bank, path const &xmlPath, bool subfolders) {
    FILE *fout = nullptr;
    _wfopen_s(&fout, xmlPath.c_str(), L"wt");
    if (!fout)
        return;
    fprintf(fout, "<Bank type=\"%u\" subID=\"%d\"", bank.type, bank.subID);
    if (bank.numSubBanks > 0)
        fprintf(fout, " numSubBanks=\"%u\"", bank.numSubBanks);
    if (bank.cycleLength > 0)
        fprintf(fout, " cycleLength=\"%u\"", bank.cycleLength);
    if (bank.sampleRepeat > 0)
        fprintf(fout, " sampleRepeat=\"%u\"", bank.sampleRepeat);
    if (subfolders) {
        unsigned char numParameters = 0;
        for (auto const &s : bank.samples) {
            if (s.parameters.size() > numParameters)
                numParameters = s.parameters.size();
        }
        if (numParameters > 0)
            fprintf(fout, " numParameters=\"%u\"", numParameters);
    }
    if (!bank.samples.empty() || subfolders) {
        if (subfolders)
            fputs("    <Directory path=\".\" />\n", fout);
        else {
            fputs(">\n", fout);
            for (auto const &s : bank.samples) {
                fprintf(fout, "    <Sample source=\"%s\"", s.source.string().c_str());
                for (unsigned int p = 0; p < s.parameters.size(); p++)
                    fprintf(fout, " param%u=\"%u\"", p + 1, s.parameters[p]);
                fputs(" />\n", fout);
            }
            fputs("</Bank>", fout);
        }
    }
    else
        fputs(" />\n", fout);
    fclose(fout);
}

void Extract(path const &inFolder, path const &outFolder, unsigned int version, bool bigEndian, bool subfolders, bool asf, bool onlyLow, bool onlyHigh) {
    create_directories(outFolder);
    for (auto const &i : directory_iterator(inFolder)) {
        auto const &p = i.path();
        if (p.extension().string() == ".hdr" || p.extension().string() == ".HDR") {
            FILE *f = nullptr;
            _wfopen_s(&f, p.c_str(), L"rb");
            if (f) {
                wstring hdrName = p.stem().c_str();
                wstring soundExt = asf ? L".asf" : L".wav";
                Bank bank;
                bank.filePath = p;
                ReadBankHeader(bank, version, f, bigEndian);
                fclose(f);
                for (unsigned int s = 0; s < bank.samples.size(); s++) {
                    bank.samples[s].source = hdrName + L"_" + format(L"{:03}", s + 1);
                    for (auto const &p : bank.samples[s].parameters)
                        bank.samples[s].source += L"_" + to_wstring(p);
                    bank.samples[s].source += soundExt;
                }
                path outDir = outFolder;
                if (subfolders) {
                    outDir /= hdrName;
                    create_directories(outDir);
                }
                WriteBankToXml(bank, outDir / (hdrName + L".xml"), subfolders);
                path datPath = p;
                datPath.replace_extension(".dat");
                FILE *datf = nullptr;
                _wfopen_s(&datf, datPath.c_str(), L"rb");
                if (datf) {
                    fseek(datf, 0, SEEK_END);
                    auto datSize = ftell(datf);
                    fseek(datf, 0, SEEK_SET);
                    vector<unsigned char> datData(datSize);
                    fread(datData.data(), datSize, 1, datf);
                    fclose(datf);
                    for (unsigned int s = 0; s < bank.samples.size(); s++) {
                        if (onlyLow) {
                            if (bank.samples[s].parameters.empty() || bank.samples[s].parameters[0] != 0)
                                continue;
                        }
                        if (onlyHigh) {
                            if (bank.samples[s].parameters.empty() || bank.samples[s].parameters[0] != 1)
                                continue;
                        }
                        if (!asf)
                            CreateTempDirectory();
                        path outSoundPath = outDir / bank.samples[s].source;
                        path outAsfPath = asf ? outSoundPath : (TempPath() / "temp_asf.asf");
                        FILE *asff = nullptr;
                        _wfopen_s(&asff, outAsfPath.c_str(), L"wb");
                        if (asff) {
                            unsigned int blockSize = 0x100 << bank.blockSize;
                            unsigned int soundOffset = bank.samples[s].offset * blockSize;
                            unsigned int nextSoundOffset = 0;
                            if (s == (bank.samples.size() - 1))
                                nextSoundOffset = datSize;
                            else
                                nextSoundOffset = bank.samples[s + 1].offset * blockSize;
                            fwrite(&datData[bank.samples[s].offset * blockSize], nextSoundOffset - soundOffset, 1, asff);
                            fclose(asff);
                            if (!asf) {
                                // convert temp asf to wav
                                STARTUPINFOW si;
                                PROCESS_INFORMATION pi;
                                ZeroMemory(&si, sizeof(si));
                                si.cb = sizeof(si);
                                ZeroMemory(&pi, sizeof(pi));
                                static path sxPath = GetExeDir() / "tools" / "sx.exe";
                                wstring commandLine = L"\"" + sxPath.wstring() + L"\" -wave -=\"" + outSoundPath.c_str() + L"\" \"" + outAsfPath.c_str() + L"\"";
                                WCHAR wargs[2048];
                                wcscpy(wargs, commandLine.c_str());
                                if (CreateProcessW(sxPath.c_str(), wargs, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {
                                    WaitForSingleObject(pi.hProcess, INFINITE);
                                    CloseHandle(pi.hProcess);
                                    CloseHandle(pi.hThread);
                                }
                                else
                                    ErrorMessage(wstring(L"Failed to convert ") + p.c_str());
                            }
                        }
                        if (!asf)
                            DeleteTempDirectory();
                    }
                }
            }
            else
                ErrorMessage(wstring(L"Failed to open ") + p.c_str());
        }
    }
}

void Pack(path const &inFolder, path const &outFolder, unsigned int version, bool bigEndian, bool asf) {
    create_directories(outFolder);

}

int wmain(int argc, wchar_t *argv[]) {
    CommandLine cmd(argc, argv, { L"version", L"i", L"o" }, { L"extract", L"pack", L"bigEndian", L"subfolders", L"asf", L"onlyLow", L"onlyHigh" });
    path in = cmd.HasArgument(L"i") ? cmd.GetArgumentPath(L"i") : current_path();
    path out = cmd.HasArgument(L"o") ? cmd.GetArgumentPath(L"o") : in;
    if (!exists(in)) {
        ErrorMessage(L"Input folder doesn't exist");
        return 1;
    }
    int version = cmd.GetArgumentInt(L"version", 2);
    bool bigEndian = cmd.HasOption(L"bigEndian");
    bool subfolders = cmd.HasOption(L"subfolders");
    bool asf = cmd.HasOption(L"asf");
    bool onlyLow = cmd.HasOption(L"onlyLow");
    bool onlyHigh = cmd.HasOption(L"onlyHigh");
    if (version < 1 || version > 2)
        version = 2;

    if (cmd.HasOption(L"extract")) {
        Extract(in, out, version, bigEndian, subfolders, asf, onlyLow, onlyHigh);
        return 0;
    }
    else if (cmd.HasOption(L"pack")) {
        Pack(in, out, version, bigEndian, subfolders, asf);
        return 0;
    }
    else {
        out = cmd.GetArgumentPath(L"o");
        if (!out.empty())
            create_directories(out);
        else
            out = current_path();
    }
    struct CommentEntry {
        path filepath;
        unsigned short offset = 0;
        bool high = false;
    };
    map<int, vector<CommentEntry>> comments;
    for (auto const &p : directory_iterator(in)) {
        auto const &path = p.path();
        wstring extensionCheck = asf ? L".asf" : L".wav";
        wstring ext = cmd.ToLower(path.extension().wstring());
        if (ext == extensionCheck) {
            auto filename = path.stem().wstring();
            int playerId = 0;
            if (filename.starts_with(L"PLAYER_NAMES_"))
                playerId = _wtoi(filename.substr(13).c_str());
            else {
                auto parts = Split(filename, L'_', false, false, false);
                if (parts.size() >= 3) {
                    try {
                        playerId = stoi(parts[parts.size() - 2]);
                    }
                    catch (...) {}
                }
            }
            if (playerId > 0) {
                CommentEntry entry;
                entry.filepath = path;
                if (filename.find(L"_HIGH") != string::npos)
                    entry.high = true;
                comments[playerId].push_back(entry);
            }
        }
    }
    auto getHeaderSize = [version](unsigned char numSounds) {
        unsigned char customDataSize = 1;
        unsigned int a = (numSounds * 3) % 4;
        if (a != 0)
            a = 4 - a;
        return (2 + customDataSize) * numSounds + a + ((version == 1) ? 16 : 20);
    };
    auto writeHeader = [version](FILE *f, int id, vector<CommentEntry> const &files, unsigned short soundsSize, unsigned int flags) {
        static unsigned short soundType = 0x3CB;
        fwrite(&soundType, 2, 1, f);
        static unsigned char customDataSize = 1;
        if (version == 1)
            fwrite(&id, 2, 1, f);
        fwrite(&customDataSize, 1, 1, f);
        unsigned char numSounds = files.size();
        fwrite(&numSounds, 1, 1, f);
        if (version == 2)
            fwrite(&id, 4, 1, f);
        static unsigned char unknown1 = 0;
        fwrite(&unknown1, 1, 1, f);
        static unsigned char offsetMultiplier = 0;
        fwrite(&offsetMultiplier, 1, 1, f);
        fwrite(&soundsSize, 2, 1, f);
        static unsigned int unknown2 = 0;
        fwrite(&unknown2, (version == 1) ? 2 : 4, 1, f);
        for (auto const &c : files) {
            unsigned short offset = _byteswap_ushort(c.offset);
            fwrite(&offset, 2, 1, f);
            fwrite(&c.high, 1, 1, f);
        }
        unsigned int a = (files.size() * 3) % 4;
        if (a != 0) {
            a = 4 - a;
            static unsigned char align = 0x70;
            for (unsigned int i = 0; i < a; i++)
                fwrite(&align, 1, 1, f);
        }
        fwrite(&flags, 4, 1, f);
    };
    auto writeAlignment = [](FILE *f, unsigned int nonAlignedSize) {
        static unsigned char zero = 0;
        unsigned int a = nonAlignedSize % 0x100;
        if (a != 0) {
            a = 0x100 - a;
            for (unsigned int i = 0; i < a; i++)
                fwrite(&zero, 1, 1, f);
        }
    };
    for (auto &[id, files] : comments) {
        sort(files.begin(), files.end(), [](CommentEntry const &a, CommentEntry const &b) {
            return a.filepath.string() < b.filepath.string();
            });
        static char pnfilename[32];
        sprintf_s(pnfilename, "pn%05d", id);
        unsigned int flags = 0;
        for (auto const &c : files) {
            if (c.high)
                flags |= 2;
            else
                flags |= 1;
        }
        unsigned int lastOffset = 0;
        {
            FILE *f = nullptr;
            _wfopen_s(&f, (out / (string(pnfilename) + ".dat")).c_str(), L"wb");
            if (f) {
                for (auto &c : files) {
                    auto filePath = c.filepath;
                    if (!asf) {
                        CreateTempDirectory();
                        path tempAsfPath = TempPath() / "temp_asf.asf";
                        // convert wav to temp asf
                        STARTUPINFOW si;
                        PROCESS_INFORMATION pi;
                        ZeroMemory(&si, sizeof(si));
                        si.cb = sizeof(si);
                        ZeroMemory(&pi, sizeof(pi));
                        static path sxPath = GetExeDir() / "tools" / "sx.exe";
                        wstring commandLine = sxPath.wstring() + L" -pcstream -=\"" + tempAsfPath.c_str() + L"\" \"" + filePath.c_str() + L"\"";
                        WCHAR wargs[2048];
                        wcscpy(wargs, commandLine.c_str());
                        if (CreateProcessW(sxPath.c_str(), wargs, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {
                            WaitForSingleObject(pi.hProcess, INFINITE);
                            CloseHandle(pi.hProcess);
                            CloseHandle(pi.hThread);
                        }
                        else
                            ErrorMessage(wstring(L"Failed to convert ") + filePath.c_str());
                        filePath = tempAsfPath;
                    }
                    FILE *cf = nullptr;
                    _wfopen_s(&cf, filePath.c_str(), L"rb");
                    if (cf) {
                        fseek(cf, 0, SEEK_END);
                        unsigned int filesize = ftell(cf);
                        c.offset = lastOffset;
                        vector<unsigned char> cdata(filesize);
                        fseek(cf, 0, SEEK_SET);
                        fread(cdata.data(), filesize, 1, cf);
                        fclose(cf);
                        fwrite(cdata.data(), filesize, 1, f);
                        writeAlignment(f, filesize);
                        unsigned int a = filesize % 0x100;
                        if (a != 0)
                            a = 0x100 - a;
                        lastOffset += (filesize + a) / 0x100;
                    }
                    else
                        ErrorMessage(wstring(L"Failed to open ") + filePath.c_str());
                    if (!asf)
                        DeleteTempDirectory();
                }
                unsigned int headerSize = getHeaderSize(files.size());
                writeAlignment(f, headerSize + 12);
                fwrite("HrIn", 4, 1, f);
                writeHeader(f, id, files, lastOffset, flags);
                fwrite("HrSz", 4, 1, f);
                fwrite(&headerSize, 4, 1, f);
                fclose(f);
            }
        }
        {
            FILE *f = nullptr;
            _wfopen_s(&f, (out / (string(pnfilename) + ".hdr")).c_str(), L"wb");
            if (f) {
                writeHeader(f, id, files, lastOffset, flags);
                fclose(f);
            }
        }
    }
}
