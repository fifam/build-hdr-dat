#include "converter.h"
#include <vector>

/*
void ConvertFile(path const &hdrFilePath, path const &outDirectory, unsigned int inVersion, bool inIsConsole, unsigned int outVersion) {
    path out;
    if (outDirectory.empty())
        out = hdrFilePath.has_parent_path() ? hdrFilePath.parent_path() : current_path();
    else {
        create_directories(outDirectory);
        out = outDirectory;
    }
    wstring hdrName = hdrFilePath.filename().c_str();
    FILE *hdr = _wfopen(hdrFilePath.c_str(), L"rb");
    if (hdr) {
        auto header = ReadHeader(hdr, inVersion, inIsConsole);
        fclose(hdr);
        path outHdrPath = outDirectory / hdrFilePath.filename();
        FILE *outHdr = _wfopen(outHdrPath.c_str(), L"wb");
        if (outHdr) {
            WriteHeader(header, outVersion, outHdr, false);
            fclose(outHdr);
            path datPath = hdrFilePath;
            datPath.replace_extension(".dat");
            if (exists(datPath)) {
                FILE *dat = _wfopen(datPath.c_str(), L"rb");
                if (dat) {
                    fseek(dat, 0, SEEK_END);
                    int datSize = ftell(dat);
                    fseek(dat, 0, SEEK_SET);
                    vector<char> datData(datSize, 0);
                    fread(datData.data(), datSize, 1, dat);
                    fclose(dat);
                    int headerOffset = -1;
                    for (int i = datSize - 4; i >= 0; i--) {
                        if (!strncmp(&datData[i], "HrSz", 4)) {
                            unsigned int headerSize = *(unsigned int *)&datData[i + 4];
                            if (inIsConsole)
                                headerSize = _byteswap_ulong(headerSize);
                            i -= headerSize + 4;
                            if (i >= 0 && !strncmp(&datData[i], "HrIn", 4))
                                headerOffset = i;
                            break;
                        }
                    }
                    if (headerOffset != -1) {
                        path outDatPath = outDirectory / datPath.filename();
                        FILE *outDat = _wfopen(outDatPath.c_str(), L"wb");
                        if (outDat) {
                            fwrite(datData.data(), headerOffset, 1, outDat);
                            WriteHeader(header, outVersion, outDat, true);
                            fclose(outDat);
                        }
                        else
                            ErrorMessage(L"Failed to write DAT for HDR: " + hdrName);
                    }
                    else
                        ErrorMessage(L"Failed to locate header in DAT for HDR: " + hdrName);
                }
                else
                    ErrorMessage(L"Failed to open DAT for HDR: " + hdrName);
            }
            else
                ErrorMessage(L"Failed to locate DAT for HDR: " + hdrName);
        }
        else
            ErrorMessage(L"Failed to write HDR: " + hdrName);
    }
    else
        ErrorMessage(L"Failed to open HDR: " + hdrName);
}
*/
